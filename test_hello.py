def test_hello_world():
    assert hello() == "Hello world"

def test_hello_marcel():
    assert hello("Marcel") == "Hello Marcel"

def hello(name="world"):
    return f"Hello {name}"

if __name__ == '__main__':
    print(hello('truc'))
